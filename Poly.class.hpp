#pragma once

# include <vector>
# include <iostream>
# include <initializer_list>

# define EPS 1e-6

class	Poly {
	public:
		Poly() {}
		Poly(Poly const&) = default;
		Poly(Poly&&) = default;
		Poly(double, unsigned int);
		Poly(float const*, std::size_t size);
		Poly(double const*, std::size_t size);
		Poly(std::initializer_list<double>);
		~Poly() {}

		double&			operator[](std::vector<double>::size_type);
		double const&	operator[](std::vector<double>::size_type) const;

		double			at(std::vector<double>::size_type);
		double const&	at(std::vector<double>::size_type) const;

		Poly	operator+(Poly const&);
		Poly	operator-(Poly const&);
		Poly	operator*(Poly const&);
		Poly	operator*(double);
		Poly	operator/(Poly const&);
		Poly	operator/(double);
		Poly	operator%(Poly const&);

		bool	operator==(Poly const&) const;

		Poly&	operator=(Poly const&) = default;

		friend std::istream&	operator>>(std::istream&, Poly&);
		friend std::ostream&	operator<<(std::ostream&, Poly const&);

	private:
		std::vector<double>	members;

		void	fit();
		std::vector<double>::size_type	getDegree(void) const;
		bool	isNull() const;
};
