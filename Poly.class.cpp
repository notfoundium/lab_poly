#include "Poly.class.hpp"

std::vector<double>::size_type	Poly::getDegree(void) const {
	return (members.size() == 0 ? (0) : (members.size() - 1));
}

bool	Poly::isNull() const {
	return (members.size() == 0);
}

void	Poly::fit() {
	while (members.size() > 0 && std::abs(members.back()) < EPS)
		members.pop_back();
}

Poly::Poly(double n, unsigned int degree) {
	for (unsigned int i = 0; i < degree; ++i) {
		members.push_back(0);
	}
	members.push_back(n);
	fit();
}

Poly::Poly(float const* c_arr, std::size_t size) {
	for (std::size_t i = 0; i < size; ++i)
		members.push_back(c_arr[i]);
	fit();
}

Poly::Poly(double const* c_arr, std::size_t size) {
	for (std::size_t i = 0; i < size; ++i)
		members.push_back(c_arr[i]);
	fit();
}

Poly::Poly(std::initializer_list<double> il) {
	std::initializer_list<double>::iterator	it = il.begin();

	for (; it != il.end(); ++it)
		members.push_back(*it);
	fit();
}

double&			Poly::operator[](std::vector<double>::size_type n) {
	return (members[n]);
}

double const&	Poly::operator[](std::vector<double>::size_type n) const {
	return (members[n]);
}

double			Poly::at(std::vector<double>::size_type n) {
	return (members.at(n));
}

double const&	Poly::at(std::vector<double>::size_type n) const {
	return (members.at(n));
}

Poly	Poly::operator+(Poly const& poly) {
	std::vector<double>				min_vec;
	std::vector<double>				max_vec;
	std::vector<double>::size_type	i = 0;
	Poly							result;

	if (members.size() < poly.members.size()) {
		min_vec = members;
		max_vec = poly.members;
	}
	else {
		max_vec = members;
		min_vec = poly.members;
	}
	for (; i < min_vec.size(); ++i)
		result.members.push_back(members[i] + poly.members[i]);
	for (; i < max_vec.size(); ++i)
		result.members.push_back(max_vec[i]);
	result.fit();
	return (result);
}

Poly	Poly::operator-(Poly const& poly) {
	std::vector<double>				min_vec;
	std::vector<double>				max_vec;
	std::vector<double>::size_type	i = 0;
	Poly							result;

	if (members.size() < poly.members.size()) {
		min_vec = members;
		max_vec = poly.members;
	}
	else {
		max_vec = members;
		min_vec = poly.members;
	}
	for (; i < min_vec.size(); ++i)
		result.members.push_back(members[i] - poly.members[i]);
	for (; i < max_vec.size(); ++i)
		result.members.push_back(max_vec[i]);
	result.fit();
	return (result);
}

Poly	Poly::operator*(double multiplier) {
	Poly	result;

	for (std::vector<double>::size_type i = 0; i < members.size(); ++i)
		result.members.push_back(members[i] * multiplier);
	return (result);
}

Poly	Poly::operator*(Poly const& poly) {
	Poly							result;
	std::vector<double>::size_type	i = 0;
	std::vector<double>::size_type	j = 0;

	for (i = 0; i < members.size() + poly.members.size() - 1; ++i)
		result.members.push_back(0);
	for (i = 0; i < members.size(); ++i)
		for (j = 0; j < poly.members.size(); ++j)
			result.members[i + j] += members[i] * poly.members[j];
	result.fit();
	return (result);
}

Poly	Poly::operator/(double divisor) {
	Poly	result;

	for (std::vector<double>::size_type i = 0; i < members.size(); ++i)
		result.members.push_back(members[i] / divisor);
	return (result);
}

Poly	Poly::operator/(Poly const& divisor) {
	Poly	tmp;
	Poly	mono;
	Poly	reminder = *this;
	Poly	result;
	size_t	i = 0;

	if (members.size() < divisor.members.size())
		return (Poly());
	while (reminder.getDegree() >= divisor.getDegree() && !reminder.isNull()) {
		tmp = Poly(reminder.members[reminder.members.size() - 1] /
		divisor.members[divisor.members.size() - 1],
		members.size() - divisor.members.size() - i);
		result = result + tmp;
		mono = Poly(result.members[result.members.size() - 1 - i],
					result.members.size() - 1 - i);
		reminder = reminder - (mono * divisor);
		result.fit();
		reminder.fit();
		++i;
	}
	return (result);
}

Poly	Poly::operator%(Poly const& divisor) {
	Poly	tmp;
	Poly	mono;
	Poly	reminder = *this;
	Poly	result;
	size_t	i = 0;

	if (members.size() < divisor.members.size())
		return (Poly());
	while (reminder.getDegree() >= divisor.getDegree() && !reminder.isNull()) {
		tmp = Poly(reminder.members[reminder.members.size() - 1] /
		divisor.members[divisor.members.size() - 1],
		members.size() - divisor.members.size() - i);
		result = result + tmp;
		mono = Poly(result.members[result.members.size() - 1 - i],
					result.members.size() - 1 - i);
		reminder = reminder - (mono * divisor);
		result.fit();
		reminder.fit();
		++i;
	}
	return (reminder);
}

bool	Poly::operator==(Poly const& poly) const {
	if (members.size() != poly.members.max_size())
		return (false);
	for (std::vector<double>::size_type i = 0; i < members.max_size(); ++i)
		if (std::abs(members[i] - poly.members[i]) > EPS)
			return (false);
	return (true);
}

std::ostream&	operator<<(std::ostream& out, Poly const& poly) {
	if (poly.members.size() > 0)
		out << poly.members[0];
	for (std::vector<double>::size_type i = 1; i < poly.members.size(); ++i) {
		if (poly.members[i] < 0)
			out << " - " << -poly.members[i] << "x";
		else
			out << " + " << poly.members[i] << "x";
		if (i != 1)
			out << "^" << i;
	}
	return (out);
}

std::istream&	operator>>(std::istream& in, Poly& poly) {
	std::size_t	size;
	double		d;

	in >> size;
	for (std::size_t i = 0; i < size; ++i) {
		in >> d;
		poly.members.push_back(d);
	}
	return (in);
}
