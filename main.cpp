#include "Poly.class.hpp"

int		main() {
	Poly	poly_1 {0, 0, 0, 0, 0, 1};
	Poly	poly_2 {1, 1};

	poly_2[1]=10;
	std::cout<<poly_2 / 2 << std::endl;

	std::cout<<poly_2 * 2 << std::endl;

	return 0;
}
